import 'package:flutter/material.dart';

class ProductControl extends StatelessWidget{
  final Function addProduct;
  ProductControl(this.addProduct);
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
          child: Text('Add'),
          color:Theme.of(context).primaryColor,
          onPressed: () {
            //createState -> setState
            addProduct('Sweet Test');
          },
          // color: Colors.amber[50],
        );
  }
}
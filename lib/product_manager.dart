import 'package:flutter/material.dart';
import './products.dart';
import './product_control.dart';

class ProductManager extends StatefulWidget {
  final String startingProduct;
  ProductManager(this.startingProduct);

  @override
  State<StatefulWidget> createState() {
    return _ProductManagerState();
  }
}
//final is cannot change it value to a new value [which means cannot assign it to new value],but not means can add in new value!!
//which means Example:
//final int a = 10;
//Then it was wrong when u write a=20;
//But u can do like this a.add() then ok
//-------------
class _ProductManagerState extends State<ProductManager> {
  List<String> _products = [];

  @override
  //super always refers to the base class extending which is STATE here
  //widget is provided by STATE object here ,and it refer to ProductManager so can call "widget.startingProduct" 
  void initState() {
    _products.add(widget.startingProduct);
    super.initState();
  }

  @override
  void didUpdateWidget(ProductManager oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  void _addProduct(String product){
    setState((){
      _products.add(product);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Container(
        margin: EdgeInsets.all(10.0),
        //u can see that there is no parameter in function passing
        //due to just pass function name to next page and let next page to pass data to here
        //here is just defined _addProduct due to just wan to define and pass function to it own page
        child: ProductControl(_addProduct)
      ),
      Products(_products),
    ]);
  }
}

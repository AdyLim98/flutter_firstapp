import 'package:flutter/material.dart';

//here is using stateless not stateful ALTHOUGH it original is inside the stateful
//due to u can see that the code below (Products class) is just for display 
//no setstate
class Products extends StatelessWidget{
  //For fix the problem of products (Pass data to here) .There had 2 solutions
  //1.constructor
  // Products(){

  // }
  //2.OR 
  final List <String> products;
  Products(this.products);
  @override
  //use final due to in the stateless wirdget and will not be changed anymore
  Widget build(BuildContext context){
  //Iterable will require map(() => components).toList()
      return Column(
              children: products.map((element) => Card(
                child: Column(
                children: <Widget>[
                  Image.asset('assets/food.jpg'),
                  Text(element)
                ],
                ),
            )).toList()
            );
  } 
}